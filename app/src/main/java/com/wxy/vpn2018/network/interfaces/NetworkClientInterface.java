package com.wxy.vpn2018.network.interfaces;


import com.wxy.vpn2018.helpers.Constants;
import com.wxy.vpn2018.models.Countries;
import com.wxy.vpn2018.models.ConfigVPN;
import com.wxy.vpn2018.models.UserInfo;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NetworkClientInterface {
    @GET(Constants.Url.GET_TOKEN)
    Single<UserInfo> getToken(@Query("lang") String lang, @Query("device") String device);

    @GET(Constants.Url.GET_COUNTRIES)
    Single<List<Countries>> getCountries(@Query("lang") String lang, @Query("device") String device);

    @GET(Constants.Url.GET_TEST)
    Single<ConfigVPN> getConfig(@Query("id_user") int id_user, @Query("token") String token, @Query("id_country") int id_country, @Query("lang") String lang, @Query("device") String device);
}
