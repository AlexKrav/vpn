package com.wxy.vpn2018.network;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.wxy.vpn2018.helpers.Constants;

import java.io.IOException;

import de.blinkt.openvpn.core.App;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        String token = sharedPreferences.getString(Constants.PreferencesConstants.TOKEN_KEY, null);

//        String tok = "Oed05-Lmq3tEiMnTbypxh3KkdfUZtnIG";
        if (token != null) {
            request = request.newBuilder()
                    .header("Authorization", token)
                    .build();
        }

        return chain.proceed(request);
    }
}
