package com.wxy.vpn2018.helpers;

import com.google.firebase.analytics.FirebaseAnalytics;

import de.blinkt.openvpn.core.App;

public class FirebaseAnalyticsManager {
    private static FirebaseAnalytics instance;

    public static FirebaseAnalytics getFirebaseAnaliticsInstance() {
        if (instance == null) {
            instance = FirebaseAnalytics.getInstance(App.getContext());
        }
        return instance;
    }

}
