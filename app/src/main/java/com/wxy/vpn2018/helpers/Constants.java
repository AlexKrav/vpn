package com.wxy.vpn2018.helpers;

public final class Constants {

    public static final String device = "android";
    public static final int russia = 1;

    public static final class Url {
        public static final String host = "https://vpn.bobrilka.ru/";
        public static final String api = "%s/api/";
        public static final String GET_TOKEN = "createToken";
        public static final String GET_COUNTRIES = "getCountries";
        public static final String GET_TEST = "getConfig";

    }

    public static final class PreferencesConstants {
        public static final String TOKEN_KEY = "token_key";
        public static final String FIRST_LOG_IN = "first_log_in";
        public static final String USER_INFO = "user_info";
        public static final String LAST_CONFIG = "last_config";
    }

    public static final class NotificationsConstants {
        public static final String CHANNEL_ID = "AndroidVpnNotification";
        public static final String CHANNEL_NAME = "AndroidVpnNotification";
    }
}
