package com.wxy.vpn2018.helpers;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.wxy.vpn2018.models.ConfigVPN;
import com.wxy.vpn2018.models.UserInfo;

import de.blinkt.openvpn.core.App;

public class SharedPreferencesHelper {
    public static String isFirstLogIn() {
        return getPreferences().getString(Constants.PreferencesConstants.FIRST_LOG_IN, null);
    }

    public static void setFirstLogIn(String firstLogIn) {
        getEditor().putString(Constants.PreferencesConstants.FIRST_LOG_IN, firstLogIn).apply();
    }

    public static void setUserInfo(UserInfo userInfo) {
        Gson gson = new Gson();
        String json = gson.toJson(userInfo);
        getEditor().putString(Constants.PreferencesConstants.USER_INFO, json).apply();
    }

    public static UserInfo getUserInfo() {
        UserInfo userInfo = null;
        Gson gson = new Gson();
        String json = getPreferences().getString(Constants.PreferencesConstants.USER_INFO, null);
        if (json != null) {
            userInfo = gson.fromJson(json, UserInfo.class);
        }
        return userInfo;
    }

    public static ConfigVPN getLastVPNConfig() {
        ConfigVPN configVPN = null;
        Gson gson = new Gson();
        String json = getPreferences().getString(Constants.PreferencesConstants.LAST_CONFIG, null);
        if (json != null) {
//            json = gson.toJson(json);
            configVPN = gson.fromJson(json, ConfigVPN.class);
        }
        return configVPN;
    }

    public static void setLastVPNConfig(ConfigVPN configVPN) {
        if (configVPN == null) {
            getEditor().putString(Constants.PreferencesConstants.LAST_CONFIG, null).apply();
        } else {
            Gson gson = new Gson();
            String json = gson.toJson(configVPN);
            getEditor().putString(Constants.PreferencesConstants.LAST_CONFIG, json).apply();
        }
    }

    private static SharedPreferences.Editor getEditor() {
        return getPreferences().edit();
    }

    private static SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(App.getContext());
    }
}
