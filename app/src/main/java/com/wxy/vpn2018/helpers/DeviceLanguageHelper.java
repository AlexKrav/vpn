package com.wxy.vpn2018.helpers;

import android.content.res.Resources;

public class DeviceLanguageHelper {
    public static String getDeviceLanguage() {
//        String lang = Resources.getSystem().getConfiguration().locale.getLanguage();
//        return lang.toUpperCase();
        return Resources.getSystem().getConfiguration().locale.getLanguage();
    }
}
