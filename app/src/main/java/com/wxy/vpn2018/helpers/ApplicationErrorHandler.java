package com.wxy.vpn2018.helpers;

import android.widget.Toast;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import de.blinkt.openvpn.core.App;
import retrofit2.HttpException;

public class ApplicationErrorHandler {

    public void showThrowable(Throwable error) {

        if (error instanceof HttpException) {
            String errorMessage = error.getMessage();
            switch (errorMessage) {
                case "HTTP 422 Data Validation Failed.":
                    Toast.makeText(App.getContext(), "HTTP 422 Data Validation Failed", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Toast.makeText(App.getContext(), "Error", Toast.LENGTH_SHORT).show();
                    break;
            }
        } else if (error instanceof SocketTimeoutException) {
            Toast.makeText(App.getContext(), "HTTP response time out", Toast.LENGTH_SHORT).show();
        } else if (error instanceof ConnectException) {
            Toast.makeText(App.getContext(), "Failed to connect", Toast.LENGTH_SHORT).show();
        } else {
            error.printStackTrace();
            Toast.makeText(App.getContext(), "Error", Toast.LENGTH_SHORT).show();
        }

    }

}
