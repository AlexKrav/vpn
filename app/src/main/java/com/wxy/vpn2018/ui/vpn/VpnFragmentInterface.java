package com.wxy.vpn2018.ui.vpn;

import com.wxy.vpn2018.models.ConfigVPN;

public interface VpnFragmentInterface {
    void readyVpnConnecting(ConfigVPN configVPN);

    void showProgress(boolean isLoading);
}
