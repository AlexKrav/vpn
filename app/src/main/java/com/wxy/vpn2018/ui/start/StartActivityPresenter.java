package com.wxy.vpn2018.ui.start;

import android.content.res.Resources;

import com.wxy.vpn2018.helpers.Constants;
import com.wxy.vpn2018.helpers.SharedPreferencesHelper;
import com.wxy.vpn2018.models.UserInfo;
import com.wxy.vpn2018.network.NetworkClient;
import com.wxy.vpn2018.network.interfaces.NetworkClientInterface;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

class StartActivityPresenter implements StartActivityPresenterInterface {
    private StartActivityInterface view;
    private Disposable disposable;

    StartActivityPresenter(StartActivityInterface view) {
        this.view = view;
    }

    @Override
    public void viewCreated() {
        if (SharedPreferencesHelper.isFirstLogIn() != null) {
            getCountries();
        } else {
            SharedPreferencesHelper.setFirstLogIn("firstLogIn");
            getToken();
            getCountries();
        }
    }

    private void getToken() {

        disposable = NetworkClient.with(NetworkClientInterface.class).getToken(getDeviceLanguage(), Constants.device)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(response -> {
//                    view.showProgress(true);

                })
                .doFinally(() -> {
                    //  view.showProgress(false);
                })
                .subscribe(this::saveUserInfo,
                        throwable -> view.showError(throwable));
    }

    private void getCountries() {
        disposable = NetworkClient.with(NetworkClientInterface.class).getCountries(getDeviceLanguage(), Constants.device)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(response -> {
//                    view.showProgressBar(true);
                })
                .subscribe(response -> {
                    view.showCountriesList(response);
//                    view.showProgressBar(false);
                }, throwable -> {
                    throwable.printStackTrace();
//                    view.showProgressBar(false);
                    view.showError(throwable);
                });
    }

    private void saveUserInfo(UserInfo response) {
        SharedPreferencesHelper.setUserInfo(response);
    }

    private String getDeviceLanguage() {
        String lang = Resources.getSystem().getConfiguration().locale.getLanguage();
        return lang.toUpperCase();
//        return Resources.getSystem().getConfiguration().locale.getLanguage();
    }

    @Override
    public void destroyed() {
        if (disposable != null && !disposable.isDisposed()) disposable.dispose();
    }
}
