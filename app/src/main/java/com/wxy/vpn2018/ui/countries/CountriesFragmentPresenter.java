package com.wxy.vpn2018.ui.countries;

import android.content.res.Resources;
import android.os.Bundle;

import com.wxy.vpn2018.R;
import com.wxy.vpn2018.helpers.Constants;
import com.wxy.vpn2018.helpers.FirebaseAnalyticsManager;
import com.wxy.vpn2018.helpers.SharedPreferencesHelper;
import com.wxy.vpn2018.models.Countries;
import com.wxy.vpn2018.models.UserInfo;
import com.wxy.vpn2018.network.NetworkClient;
import com.wxy.vpn2018.network.interfaces.NetworkClientInterface;
import com.wxy.vpn2018.sharedViewModel.SharedViewModel;

import java.io.InputStream;

import de.blinkt.openvpn.core.App;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

class CountriesFragmentPresenter implements CountriesFragmentPresenterInterface {
    private CountriesFragmentInterface view;
    private Disposable disposable;
    private InputStream inputStream;
    private SharedViewModel sharedViewModel;

    CountriesFragmentPresenter(CountriesFragmentInterface view) {
        this.view = view;
    }

    @Override
    public void onItemClicked(Countries country) {
        setFirebaseAnalytics(country);
        UserInfo user = SharedPreferencesHelper.getUserInfo();
        disposable = NetworkClient.with(NetworkClientInterface.class).getConfig(user.getId_user(), user.getToken(), country.getId_country(), getDeviceLanguage(), Constants.device)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(response -> {
                    view.showProgress(true);
                })
                .doFinally(() -> {
                    view.showProgress(false);
                })
                .subscribe((response -> {

                    view.showSelectedCountry(country, response);

                }));
//        view.showSelectedCountry(country);
    }

    private void setFirebaseAnalytics(Countries country) {
        Bundle bundle = new Bundle();
        bundle.putString(App.getContext().getString(R.string.country_clicked), country.getName());
        FirebaseAnalyticsManager.getFirebaseAnaliticsInstance().logEvent(App.getContext().getString(R.string.country_analytics), bundle);
    }

    private String getDeviceLanguage() {
//        String lang = Resources.getSystem().getConfiguration().locale.getLanguage();
//        return lang.toUpperCase();
        return Resources.getSystem().getConfiguration().locale.getLanguage();
    }
}
