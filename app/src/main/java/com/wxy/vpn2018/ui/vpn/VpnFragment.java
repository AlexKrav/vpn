package com.wxy.vpn2018.ui.vpn;

import android.arch.lifecycle.ViewModelProviders;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.wxy.vpn2018.DataCleanManager;
import com.wxy.vpn2018.ProfileAsync;
import com.wxy.vpn2018.R;
import com.wxy.vpn2018.databinding.FragmentVpnBinding;
import com.wxy.vpn2018.models.ConfigVPN;
import com.wxy.vpn2018.sharedViewModel.SharedViewModel;
import com.wxy.vpn2018.ui.countries.CountriesFragment;

import de.blinkt.openvpn.LaunchVPN;
import de.blinkt.openvpn.VpnProfile;
import de.blinkt.openvpn.core.App;
import de.blinkt.openvpn.core.ConnectionStatus;
import de.blinkt.openvpn.core.IOpenVPNServiceInternal;
import de.blinkt.openvpn.core.OpenVPNService;
import de.blinkt.openvpn.core.ProfileManager;
import de.blinkt.openvpn.core.VpnStatus;

public class VpnFragment extends Fragment implements VpnFragmentInterface, VpnStatus.StateListener, VpnStatus.ByteCountListener {
    private FragmentVpnBinding binding;
    private ProfileAsync profileAsync;
    SharedViewModel sharedViewModel;
    private IOpenVPNServiceInternal mService;
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = IOpenVPNServiceInternal.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_vpn, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupListeners();
        setupSharedViewModel();
        if (!App.isStart) {
            DataCleanManager.cleanCache(getContext());
            binding.btnConnect.setEnabled(false);
            binding.progressBar.setVisibility(View.VISIBLE);
            profileAsync = new ProfileAsync(getContext(), new ProfileAsync.OnProfileLoadListener() {
                @Override
                public void onProfileLoadSuccess() {
                    binding.progressBar.setVisibility(View.GONE);
                    binding.btnConnect.setEnabled(true);
                }

                @Override
                public void onProfileLoadFailed(String msg) {
                    binding.progressBar.setVisibility(View.GONE);
                    Toast.makeText(getContext(), requireActivity().getString(R.string.init_fail) + msg, Toast.LENGTH_SHORT).show();
                }
            });
            profileAsync.execute("z");
//            rate();
        }
    }

    private void setupSharedViewModel() {
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel.class);
        sharedViewModel.getCountryMutableLiveData().observe(this, country -> {
            binding.selectCountry.tvCurrentCountry.setText(country.getName());
            Glide.with(requireContext())
                    .load(country.getLogo())
                    .into(binding.selectCountry.ivCountry);
        });
        sharedViewModel.getInputStreamMutableLiveData().observe(this, configVPN -> {
            binding.progressBar.setVisibility(View.VISIBLE);
            profileAsync = new ProfileAsync(getContext(), new ProfileAsync.OnProfileLoadListener() {
                @Override
                public void onProfileLoadSuccess() {
                    binding.progressBar.setVisibility(View.GONE);
                    binding.btnConnect.setEnabled(true);
                }

                @Override
                public void onProfileLoadFailed(String msg) {
                    binding.progressBar.setVisibility(View.GONE);
                    Toast.makeText(getContext(), requireActivity().getString(R.string.init_fail) + msg, Toast.LENGTH_SHORT).show();
                }
            });
            profileAsync.execute(configVPN.getConfig());
        });
    }

    @Override
    public void onStart() {
        super.onStart();
//        mHandler = new Handler(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        requireActivity().unbindService(mConnection);
    }

    @Override
    public void onResume() {
        super.onResume();
        VpnStatus.addStateListener(this);
        VpnStatus.addByteCountListener(this);
        Intent intent = new Intent(getActivity(), OpenVPNService.class);
        intent.setAction(OpenVPNService.START_SERVICE);
        requireActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        VpnStatus.removeStateListener(this);
        VpnStatus.removeByteCountListener(this);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (profileAsync != null && !profileAsync.isCancelled()) {
            profileAsync.cancel(true);
        }
    }

    private void setupListeners() {
        binding.selectCountry.currentCountry.setOnClickListener(v -> {
            CountriesFragment countriesFragment = new CountriesFragment();
            countriesFragment.setEnterTransition(new Slide());
            countriesFragment.setExitTransition(new Slide());
            requireActivity().getSupportFragmentManager().beginTransaction().add(R.id.container, countriesFragment).addToBackStack(CountriesFragment.class.getName()).commit();
        });

        binding.btnConnect.setOnClickListener(view -> {
            Runnable r = () -> {
                if (!App.isStart) {
                    startVPN();
                    App.isStart = true;
                } else {
                    stopVPN();
                    App.isStart = false;
                }
            };
            r.run();
        });

//        binding.btnConnect.setOnClickListener(v -> {
//            try {
//                prepareStartProfile(START_PROFILE_BYUUID);
//            } catch (RemoteException e) {
//                e.printStackTrace();
//            }
//        });
    }

    void startVPN() {
        binding.progressBar.setVisibility(View.VISIBLE);
        try {
            ProfileManager pm = ProfileManager.getInstance(getContext());
            VpnProfile profile = pm.getProfileByName(Build.MODEL);//
            startVPNConnection(profile);
        } catch (Exception ex) {
            App.isStart = false;
        }
    }

    public void startVPNConnection(VpnProfile vp) {
        Intent intent = new Intent(requireActivity().getApplicationContext(), LaunchVPN.class);
        intent.putExtra(LaunchVPN.EXTRA_KEY, vp.getUUID().toString());
        intent.setAction(Intent.ACTION_MAIN);
        startActivity(intent);
    }

    void stopVPN() {
        stopVPNConnection();
        binding.progressBar.setVisibility(View.GONE);

        binding.tvConnect.setText(getString(R.string.connect));
    }

    public void stopVPNConnection() {
        ProfileManager.setConntectedVpnProfileDisconnected(getContext());
        if (mService != null) {
            try {
                mService.stopVPN(false);
            } catch (RemoteException e) {
//                VpnStatus.logException(e);
            }
        }
    }

    @Override
    public void updateState(String state, String logmessage, int localizedResId, ConnectionStatus level) {
        requireActivity().runOnUiThread(() -> {
            if (state.equals("CONNECTED")) {
                App.isStart = true;
                setConnected();
            }
            if (state.equals("AUTH_FAILED")) {
                Toast.makeText(getContext(), "Wrong Username or Password!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void setConnected() {
        binding.progressBar.setVisibility(View.GONE);
        binding.tvConnect.setText("connected");
    }

    @Override
    public void setConnectedVPN(String uuid) {

    }

    @Override
    public void updateByteCount(long in, long out, long diffIn, long diffOut) {

    }

    @Override
    public void readyVpnConnecting(ConfigVPN configVPN) {

    }

    @Override
    public void showProgress(boolean isLoading) {

    }
}
