package com.wxy.vpn2018.ui.vpn;

interface VpnFragmentPresenterInterface {
    void viewCreated();
    void onPause();
    void onResume();
    void onStop();
    void onDestroy();
}
