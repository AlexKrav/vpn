package com.wxy.vpn2018.ui.countries;

import com.wxy.vpn2018.models.Countries;

interface CountriesFragmentPresenterInterface {
    void onItemClicked(Countries item);
}
