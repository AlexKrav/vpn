package com.wxy.vpn2018.ui.start;


import com.wxy.vpn2018.models.Countries;

import java.util.List;


public interface StartActivityInterface {
    void showError(Throwable throwable);

    void showCountriesList(List<Countries> response);
}
