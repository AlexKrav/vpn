package com.wxy.vpn2018.ui.countries;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wxy.vpn2018.R;
import com.wxy.vpn2018.adapters.CountriesAdapter;
import com.wxy.vpn2018.databinding.ActivityCountriesBinding;
import com.wxy.vpn2018.models.ConfigVPN;
import com.wxy.vpn2018.models.Countries;
import com.wxy.vpn2018.sharedViewModel.SharedViewModel;

import java.util.List;

public class CountriesFragment extends Fragment implements CountriesFragmentInterface {
    private ActivityCountriesBinding binding;
    private CountriesAdapter adapter;
    private CountriesFragmentPresenterInterface presenter;
    private SharedViewModel sharedViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_countries, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.actionBar.getRoot().setOnClickListener(v -> getActivity().onBackPressed());
        presenter = new CountriesFragmentPresenter(this);
        setupSharedViewModel();

        Intent intent = requireActivity().getIntent();
        List<Countries> countriesList = (List<Countries>) intent.getSerializableExtra("aaa");
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        binding.rvCountries.setLayoutManager(llm);
        adapter = new CountriesAdapter(countriesList);
        binding.rvCountries.setAdapter(adapter);
        adapter.setItemClickListener((item, position) -> {
            presenter.onItemClicked(adapter.getItem(position));

//            Bundle bundle = new Bundle();
//            bundle.putString(getString(R.string.country_clicked), adapter.getItem(position).getName());
//            FirebaseAnalyticsManager.getFirebaseAnaliticsInstance().logEvent(getString(R.string.country_analytics), bundle);
        });


    }

    private void setupSharedViewModel() {
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel.class);
    }

    @Override
    public void showSelectedCountry(Countries selectedCountry,ConfigVPN configVPN) {
        sharedViewModel.setCountryMutableLiveData(selectedCountry);
        sharedViewModel.setInputStreamMutableLiveData(configVPN);
        requireActivity().onBackPressed();
    }

    @Override
    public void showProgress(boolean isLoading) {
        if (isLoading) {
            binding.progressBar.setVisibility(View.VISIBLE);
        } else binding.progressBar.setVisibility(View.GONE);
    }
}
