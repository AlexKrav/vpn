package com.wxy.vpn2018.ui.main;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.wxy.vpn2018.BaseActivity;
import com.wxy.vpn2018.R;
import com.wxy.vpn2018.databinding.ActivityMainBinding;
import com.wxy.vpn2018.helpers.ApplicationErrorHandler;
import com.wxy.vpn2018.helpers.Constants;
import com.wxy.vpn2018.helpers.FirebaseAnalyticsManager;
import com.wxy.vpn2018.models.Countries;
import com.wxy.vpn2018.ui.vpn.VpnFragment;

import java.util.List;

import de.blinkt.openvpn.core.App;

public class MainActivity extends BaseActivity implements MainActivityInterface {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main, null);
        setupDefaultFragment();
        initFirebase();

    }

    private void setupDefaultFragment() {
        getSupportFragmentManager().beginTransaction().add(R.id.container, new VpnFragment()).commit();
    }

    private void initFirebase() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(Constants.NotificationsConstants.CHANNEL_ID, Constants.NotificationsConstants.CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
        FirebaseAnalyticsManager.getFirebaseAnaliticsInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showCountriesList(List<Countries> response) {

    }

    @Override
    public void showError(Throwable throwable) {
        ApplicationErrorHandler errorHandler = App.getErrorHandler();
        errorHandler.showThrowable(throwable);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0)
            getSupportFragmentManager().popBackStack();
    }
}
