package com.wxy.vpn2018.ui.main;

public interface MainActivityPresenterInterface {
    void viewCreated();
    void destroyed();
}
