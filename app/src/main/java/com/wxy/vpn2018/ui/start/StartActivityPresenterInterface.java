package com.wxy.vpn2018.ui.start;

interface StartActivityPresenterInterface {
    void viewCreated();
    void destroyed();
}
