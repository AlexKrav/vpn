package com.wxy.vpn2018.ui.main;


import io.reactivex.disposables.Disposable;

class MainActivityPresenter implements MainActivityPresenterInterface {
    private MainActivityInterface view;
    private Disposable disposable;

    MainActivityPresenter(MainActivityInterface view) {
        this.view = view;
    }

    @Override
    public void viewCreated() {
    }

    @Override
    public void destroyed() {
        if (disposable != null && !disposable.isDisposed()) disposable.dispose();
    }
}
