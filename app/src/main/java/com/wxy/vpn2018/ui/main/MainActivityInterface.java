package com.wxy.vpn2018.ui.main;

import com.wxy.vpn2018.models.Countries;

import java.util.List;

public interface MainActivityInterface {
    void showCountriesList(List<Countries> response);

    void showError(Throwable throwable);
}
