package com.wxy.vpn2018.ui.countries;

import com.wxy.vpn2018.models.ConfigVPN;
import com.wxy.vpn2018.models.Countries;

interface CountriesFragmentInterface {
    void showSelectedCountry(Countries country, ConfigVPN configVPN);

    void showProgress(boolean isLoading);
}
