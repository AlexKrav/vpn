package com.wxy.vpn2018.ui.start;

import android.content.Intent;
import android.os.Bundle;

import com.wxy.vpn2018.BaseActivity;
import com.wxy.vpn2018.R;
import com.wxy.vpn2018.models.Countries;
import com.wxy.vpn2018.ui.main.MainActivity;

import java.io.Serializable;
import java.util.List;

public class StartActivity extends BaseActivity implements StartActivityInterface {
    private StartActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        presenter = new StartActivityPresenter(this);
        presenter.viewCreated();
//        new Handler().postDelayed(() -> {
//            final Intent mainIntent = new Intent(StartActivity.this, MainActivityVPN.class);
//            startActivity(mainIntent);
//            StartActivity.this.finish();
//        }, 1000);
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void showCountriesList(List<Countries> response) {
        Intent intent = new Intent(StartActivity.this, MainActivity.class);
        intent.putExtra("aaa", (Serializable) response);
        startActivity(intent);
        StartActivity.this.finish();
    }
}
