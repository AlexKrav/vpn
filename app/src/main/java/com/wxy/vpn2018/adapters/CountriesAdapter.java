package com.wxy.vpn2018.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.wxy.vpn2018.R;
import com.wxy.vpn2018.databinding.CountryItemBinding;
import com.wxy.vpn2018.models.Countries;

import java.util.List;

import de.blinkt.openvpn.core.App;

public class CountriesAdapter extends BaseAdapter<Countries, CountriesAdapter.Holder> {

    public CountriesAdapter(List<Countries> items) {
        super(items);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        CountryItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.country_item, viewGroup, false);
        return new Holder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        Countries country = getItem(i);
        holder.binding.setCountry(country);
        Glide.with(App.getContext())
                .load(country.getLogo())
                .into(holder.binding.imgCountry);
        setMarginForLastItem(holder, 4);
        holder.binding.countryItem.setOnClickListener(v -> {
            getItemClickListener().onItemClick(holder.binding.getRoot(), i);
        });
    }

    private void setMarginForLastItem(Holder holder, int dp) {
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) holder.itemView.getLayoutParams();
        int marginBottom = convertDpToPixel(dp, App.getContext());
        layoutParams.setMargins(0, 0, 0, marginBottom);
        holder.itemView.setLayoutParams(layoutParams);


    }

    private static int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return (int) (dp * (metrics.densityDpi / 160f));
    }


    class Holder extends RecyclerView.ViewHolder {
        CountryItemBinding binding;

        Holder(@NonNull CountryItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
