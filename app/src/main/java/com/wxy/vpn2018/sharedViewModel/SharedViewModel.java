package com.wxy.vpn2018.sharedViewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wxy.vpn2018.models.ConfigVPN;
import com.wxy.vpn2018.models.Countries;

public class SharedViewModel extends ViewModel {

    private MutableLiveData<Countries> countryMutableLiveData;
    private MutableLiveData<ConfigVPN> inputStreamMutableLiveData;

    public LiveData<Countries> getCountryMutableLiveData() {
        if (countryMutableLiveData == null) {
            countryMutableLiveData = new MutableLiveData<>();
        }
        return countryMutableLiveData;
    }

    public void setCountryMutableLiveData(Countries country) {
        countryMutableLiveData.setValue(country);
    }

    public LiveData<ConfigVPN> getInputStreamMutableLiveData() {
        if (inputStreamMutableLiveData == null) {
            inputStreamMutableLiveData = new MutableLiveData<>();
        }
        return inputStreamMutableLiveData;
    }

    public void setInputStreamMutableLiveData(ConfigVPN newInputStream) {
        inputStreamMutableLiveData.setValue(newInputStream);
    }


}
