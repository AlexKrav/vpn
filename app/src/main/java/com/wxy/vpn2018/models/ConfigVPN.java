package com.wxy.vpn2018.models;

import java.io.Serializable;

public class ConfigVPN implements Serializable {
    private int id_country;
    private String config;

    public int getId_country() {
        return id_country;
    }

    public void setId_country(int id_country) {
        this.id_country = id_country;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }
}
