package com.wxy.vpn2018.databinding;

import android.databinding.Bindable;
import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.wxy.vpn2018.models.Countries;
import de.hdodenhof.circleimageview.CircleImageView;

public abstract class CountryItemBinding extends ViewDataBinding {
  @NonNull
  public final ImageView arrowForward;

  @NonNull
  public final TextView country;

  @NonNull
  public final ConstraintLayout countryItem;

  @NonNull
  public final CircleImageView imgCountry;

  @Bindable
  protected Countries mCountry;

  protected CountryItemBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, ImageView arrowForward, TextView country, ConstraintLayout countryItem,
      CircleImageView imgCountry) {
    super(_bindingComponent, _root, _localFieldCount);
    this.arrowForward = arrowForward;
    this.country = country;
    this.countryItem = countryItem;
    this.imgCountry = imgCountry;
  }

  public abstract void setCountry(@Nullable Countries country);

  @Nullable
  public Countries getCountry() {
    return mCountry;
  }

  @NonNull
  public static CountryItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static CountryItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<CountryItemBinding>inflate(inflater, com.wxy.vpn2018.R.layout.country_item, root, attachToRoot, component);
  }

  @NonNull
  public static CountryItemBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static CountryItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<CountryItemBinding>inflate(inflater, com.wxy.vpn2018.R.layout.country_item, null, false, component);
  }

  public static CountryItemBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static CountryItemBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (CountryItemBinding)bind(component, view, com.wxy.vpn2018.R.layout.country_item);
  }
}
