package com.wxy.vpn2018.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public abstract class BlockCurrentCountryBinding extends ViewDataBinding {
  @NonNull
  public final ImageView arrowDown;

  @NonNull
  public final ConstraintLayout currentCountry;

  @NonNull
  public final ImageView ivCountry;

  @NonNull
  public final TextView tvCurrentCountry;

  protected BlockCurrentCountryBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, ImageView arrowDown, ConstraintLayout currentCountry,
      ImageView ivCountry, TextView tvCurrentCountry) {
    super(_bindingComponent, _root, _localFieldCount);
    this.arrowDown = arrowDown;
    this.currentCountry = currentCountry;
    this.ivCountry = ivCountry;
    this.tvCurrentCountry = tvCurrentCountry;
  }

  @NonNull
  public static BlockCurrentCountryBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static BlockCurrentCountryBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<BlockCurrentCountryBinding>inflate(inflater, com.wxy.vpn2018.R.layout.block_current_country, root, attachToRoot, component);
  }

  @NonNull
  public static BlockCurrentCountryBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static BlockCurrentCountryBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<BlockCurrentCountryBinding>inflate(inflater, com.wxy.vpn2018.R.layout.block_current_country, null, false, component);
  }

  public static BlockCurrentCountryBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static BlockCurrentCountryBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (BlockCurrentCountryBinding)bind(component, view, com.wxy.vpn2018.R.layout.block_current_country);
  }
}
