package com.wxy.vpn2018.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public abstract class FragmentVpnBinding extends ViewDataBinding {
  @NonNull
  public final ImageView beaver;

  @NonNull
  public final ImageView btnConnect;

  @NonNull
  public final ProgressBar progressBar;

  @NonNull
  public final BlockCurrentCountryBinding selectCountry;

  @NonNull
  public final TextView titleVpn;

  @NonNull
  public final TextView tvConnect;

  protected FragmentVpnBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, ImageView beaver, ImageView btnConnect, ProgressBar progressBar,
      BlockCurrentCountryBinding selectCountry, TextView titleVpn, TextView tvConnect) {
    super(_bindingComponent, _root, _localFieldCount);
    this.beaver = beaver;
    this.btnConnect = btnConnect;
    this.progressBar = progressBar;
    this.selectCountry = selectCountry;
    setContainedBinding(this.selectCountry);;
    this.titleVpn = titleVpn;
    this.tvConnect = tvConnect;
  }

  @NonNull
  public static FragmentVpnBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentVpnBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentVpnBinding>inflate(inflater, com.wxy.vpn2018.R.layout.fragment_vpn, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentVpnBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentVpnBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentVpnBinding>inflate(inflater, com.wxy.vpn2018.R.layout.fragment_vpn, null, false, component);
  }

  public static FragmentVpnBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentVpnBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentVpnBinding)bind(component, view, com.wxy.vpn2018.R.layout.fragment_vpn);
  }
}
