package com.wxy.vpn2018.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public abstract class BlockShareBinding extends ViewDataBinding {
  @NonNull
  public final ImageView backgroundBottom;

  @NonNull
  public final ImageView backgroundButton;

  @NonNull
  public final TextView getPremium;

  @NonNull
  public final TextView removeAds;

  @NonNull
  public final ImageView rocket;

  protected BlockShareBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, ImageView backgroundBottom, ImageView backgroundButton,
      TextView getPremium, TextView removeAds, ImageView rocket) {
    super(_bindingComponent, _root, _localFieldCount);
    this.backgroundBottom = backgroundBottom;
    this.backgroundButton = backgroundButton;
    this.getPremium = getPremium;
    this.removeAds = removeAds;
    this.rocket = rocket;
  }

  @NonNull
  public static BlockShareBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static BlockShareBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<BlockShareBinding>inflate(inflater, com.wxy.vpn2018.R.layout.block_share, root, attachToRoot, component);
  }

  @NonNull
  public static BlockShareBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static BlockShareBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<BlockShareBinding>inflate(inflater, com.wxy.vpn2018.R.layout.block_share, null, false, component);
  }

  public static BlockShareBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static BlockShareBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (BlockShareBinding)bind(component, view, com.wxy.vpn2018.R.layout.block_share);
  }
}
