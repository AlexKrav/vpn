package com.wxy.vpn2018.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public abstract class CountryActionBarBinding extends ViewDataBinding {
  @NonNull
  public final ImageView arrow;

  @NonNull
  public final ConstraintLayout backArrow;

  @NonNull
  public final TextView backText;

  protected CountryActionBarBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, ImageView arrow, ConstraintLayout backArrow, TextView backText) {
    super(_bindingComponent, _root, _localFieldCount);
    this.arrow = arrow;
    this.backArrow = backArrow;
    this.backText = backText;
  }

  @NonNull
  public static CountryActionBarBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static CountryActionBarBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<CountryActionBarBinding>inflate(inflater, com.wxy.vpn2018.R.layout.country_action_bar, root, attachToRoot, component);
  }

  @NonNull
  public static CountryActionBarBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static CountryActionBarBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<CountryActionBarBinding>inflate(inflater, com.wxy.vpn2018.R.layout.country_action_bar, null, false, component);
  }

  public static CountryActionBarBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static CountryActionBarBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (CountryActionBarBinding)bind(component, view, com.wxy.vpn2018.R.layout.country_action_bar);
  }
}
