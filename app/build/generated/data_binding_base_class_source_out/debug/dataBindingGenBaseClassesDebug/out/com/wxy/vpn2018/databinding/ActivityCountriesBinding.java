package com.wxy.vpn2018.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

public abstract class ActivityCountriesBinding extends ViewDataBinding {
  @NonNull
  public final CountryActionBarBinding actionBar;

  @NonNull
  public final ProgressBar progressBar;

  @NonNull
  public final RecyclerView rvCountries;

  protected ActivityCountriesBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, CountryActionBarBinding actionBar, ProgressBar progressBar,
      RecyclerView rvCountries) {
    super(_bindingComponent, _root, _localFieldCount);
    this.actionBar = actionBar;
    setContainedBinding(this.actionBar);;
    this.progressBar = progressBar;
    this.rvCountries = rvCountries;
  }

  @NonNull
  public static ActivityCountriesBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityCountriesBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityCountriesBinding>inflate(inflater, com.wxy.vpn2018.R.layout.activity_countries, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityCountriesBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityCountriesBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityCountriesBinding>inflate(inflater, com.wxy.vpn2018.R.layout.activity_countries, null, false, component);
  }

  public static ActivityCountriesBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static ActivityCountriesBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (ActivityCountriesBinding)bind(component, view, com.wxy.vpn2018.R.layout.activity_countries);
  }
}
