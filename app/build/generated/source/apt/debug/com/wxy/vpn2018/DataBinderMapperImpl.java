package com.wxy.vpn2018;

import android.databinding.DataBinderMapper;
import android.databinding.DataBindingComponent;
import android.databinding.ViewDataBinding;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import com.wxy.vpn2018.databinding.ActivityCountriesBindingImpl;
import com.wxy.vpn2018.databinding.ActivityMainBindingImpl;
import com.wxy.vpn2018.databinding.BlockCurrentCountryBindingImpl;
import com.wxy.vpn2018.databinding.BlockShareBindingImpl;
import com.wxy.vpn2018.databinding.CountryActionBarBindingImpl;
import com.wxy.vpn2018.databinding.CountryItemBindingImpl;
import com.wxy.vpn2018.databinding.FragmentVpnBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYCOUNTRIES = 1;

  private static final int LAYOUT_ACTIVITYMAIN = 2;

  private static final int LAYOUT_BLOCKCURRENTCOUNTRY = 3;

  private static final int LAYOUT_BLOCKSHARE = 4;

  private static final int LAYOUT_COUNTRYACTIONBAR = 5;

  private static final int LAYOUT_COUNTRYITEM = 6;

  private static final int LAYOUT_FRAGMENTVPN = 7;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(7);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.wxy.vpn2018.R.layout.activity_countries, LAYOUT_ACTIVITYCOUNTRIES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.wxy.vpn2018.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.wxy.vpn2018.R.layout.block_current_country, LAYOUT_BLOCKCURRENTCOUNTRY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.wxy.vpn2018.R.layout.block_share, LAYOUT_BLOCKSHARE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.wxy.vpn2018.R.layout.country_action_bar, LAYOUT_COUNTRYACTIONBAR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.wxy.vpn2018.R.layout.country_item, LAYOUT_COUNTRYITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.wxy.vpn2018.R.layout.fragment_vpn, LAYOUT_FRAGMENTVPN);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYCOUNTRIES: {
          if ("layout/activity_countries_0".equals(tag)) {
            return new ActivityCountriesBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_countries is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYMAIN: {
          if ("layout/activity_main_0".equals(tag)) {
            return new ActivityMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
        }
        case  LAYOUT_BLOCKCURRENTCOUNTRY: {
          if ("layout/block_current_country_0".equals(tag)) {
            return new BlockCurrentCountryBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for block_current_country is invalid. Received: " + tag);
        }
        case  LAYOUT_BLOCKSHARE: {
          if ("layout/block_share_0".equals(tag)) {
            return new BlockShareBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for block_share is invalid. Received: " + tag);
        }
        case  LAYOUT_COUNTRYACTIONBAR: {
          if ("layout/country_action_bar_0".equals(tag)) {
            return new CountryActionBarBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for country_action_bar is invalid. Received: " + tag);
        }
        case  LAYOUT_COUNTRYITEM: {
          if ("layout/country_item_0".equals(tag)) {
            return new CountryItemBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for country_item is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTVPN: {
          if ("layout/fragment_vpn_0".equals(tag)) {
            return new FragmentVpnBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_vpn is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new com.android.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(3);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "country");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(7);

    static {
      sKeys.put("layout/activity_countries_0", com.wxy.vpn2018.R.layout.activity_countries);
      sKeys.put("layout/activity_main_0", com.wxy.vpn2018.R.layout.activity_main);
      sKeys.put("layout/block_current_country_0", com.wxy.vpn2018.R.layout.block_current_country);
      sKeys.put("layout/block_share_0", com.wxy.vpn2018.R.layout.block_share);
      sKeys.put("layout/country_action_bar_0", com.wxy.vpn2018.R.layout.country_action_bar);
      sKeys.put("layout/country_item_0", com.wxy.vpn2018.R.layout.country_item);
      sKeys.put("layout/fragment_vpn_0", com.wxy.vpn2018.R.layout.fragment_vpn);
    }
  }
}
